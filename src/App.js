import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Col, Container, Row } from 'react-bootstrap'
import MyNavbar from './components/Navbar'
import Account from './components/Account'
import MyTable from './components/MyTable'

export default class App extends Component {

    constructor(){
        super();
        this.state={
            message:{
                nameErr: "",
                emailErr: "",
                genderErr: "",
                passwordErr: ""
            },
            tempAccount:{
                name: "",
                email: "",
                gender: "male",
                password: "",
                select : false
            },
            account: [
                {
                    name: "Dara",
                    email: "dara@gmail.com",
                    gender: "male",
                    password: "123456",
                    select : false
                },
                {
                    name: "Nana",
                    email: "nana@gmail.com",
                    gender: "female",
                    password: "654321",
                    select : false
                },
                {
                    name: "Lina",
                    email: "lina@gmail.com",
                    gender: "female",
                    password: "987654",
                    select : false
                }
            ]
        }
    }

    onTextChange = (e) => {
        let temp = {...this.state.tempAccount}
        if(e.target.id === "formBasicName"){
            let name = e.target.value
            temp.name = name
            let err = this.state.message
            if(name === ""){
                err.nameErr = "Can Not Be Empty!"
            }else { 
                err.nameErr = " "
                temp.name = name 
            }
        }
        else if(e.target.id === "formBasicEmail"){
            let pattern = /\S+@\S+\.\S+$/
            let email = e.target.value
            let err = this.state.message
            temp.email = email
            if(email === ""){
                err.emailErr = "Can Not Be Empty!"
            }else if(pattern.test(email)){ 
                err.emailErr = " "
                temp.email = email
                
            }else{
                
                err.emailErr = "Invalid Input!"
            }
            
            this.setState({message: err})
        }
        else if(e.target.id === "formBasicPassword"){
            let password = e.target.value
            temp.password = password
            let err = this.state.message
            if(password === ""){
                err.passwordErr = "Can Not Be Empty!"
            }else { 
                err.passwordErr = " "
                temp.password = password
            }
        }
        this.setState({tempAccount: temp})
    }

    onRadioChange = (e) =>{

        let temp = {...this.state.tempAccount}
        if(e.target.id === "radios1"){
            let gender = e.target.value
            console.log(e.target.id , 'male')
            temp.gender = gender
        }
        else{
            let gender = e.target.value
            console.log(e.target.id , 'female')
            temp.gender = gender
        }
        this.setState({
            tempAccount : temp
        })

    }

    onAdd = () => {
        let temp = {...this.state.tempAccount}
        let lastTemp = [...this.state.account]
        lastTemp.push(temp)
        this.setState({account: lastTemp})  
        let tempClear = {...this.state.tempAccount}
        tempClear.name = ""
        tempClear.email = ""
        tempClear.password = ""
        this.setState({tempAccount: tempClear})

    }

    onbtnCheck = () =>{
        let b = true
        
        if(this.state.message.emailErr === "Invalid Input!" || this.state.message.emailErr === "Can Not Be Empty!"||
        this.state.message.nameErr === "" || this.state.message.emailErr === "" || this.state.message.passwordErr === "")
            b =true
        else if(this.state.message.nameErr === " " && this.state.message.emailErr === " " && this.state.message.passwordErr === " ")
            b = false
        return b
    }

    onRowSelect = (index)=>{
        let temp = this.state.account
        let sl = temp[index].select
        if(sl===false)
            sl =true
        else 
            sl = false
        temp[index].select = sl
        this.setState({
            account: temp
        })
    }

    onDelete = () =>{
        let temp = this.state.account.filter(item => item.select === false)
        this.setState({
            account: temp
        })

    }

    render() {
        return (
            <Container fluid="md" style={{margin: 'auto', width: '70%' }}>
                <MyNavbar/>
                <Row>
                    <Col md={4}>
                        <Account 
                        onAdd={this.onAdd}
                        onTextChange={this.onTextChange}
                        onRadioChange={this.onRadioChange}
                        onbtnCheck={this.onbtnCheck}
                        items={this.state.account}
                        message={this.state.message}
                        tempAccount={this.state.tempAccount}
                        />
                    </Col>
                    <Col md={8}>
                        <MyTable
                        items={this.state.account}
                        onRowSelect={this.onRowSelect}
                        onDelete={this.onDelete}
                        />
                    </Col>
                </Row>
            </Container>
        )
    }
}
