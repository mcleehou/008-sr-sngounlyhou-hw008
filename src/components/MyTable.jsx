import React from 'react'
import '../Table.css'
import { Table,Button } from 'react-bootstrap'

export default function MyTable(props) {
    return (
        <div>
            <br /><br /><br /><br /><br />
            <h3> Table Account</h3>
            <Table  bordered hover >
                <thead>
                    <tr>
                        <th>#</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Gender</th>
                    </tr>
                </thead>
                <tbody>
                    { props.items.map((item,index) => (
                    <tr key={index} onClick={()=> props.onRowSelect(index)}
                    className={item.select? "select":"" }
                    >
                        <td>{index + 1}</td>
                        <td>{item.name}</td>
                        <td>{item.email}</td>
                        <td>{item.gender}</td>
                    </tr>
                    ))
                    }
                </tbody>
            </Table>
            <br />
            <Button variant="danger"
                onClick={props.onDelete}
            >Delete</Button>
        </div>
    )
}
