import React from 'react'
import { Col, Row } from 'react-bootstrap'

export default function MyNavbar() {

    return (
        
            <Row>
                <Col xs={6} style={{  height: '50px', width: '100%', display: 'flex', justifyContent: 'space-between',alignItems: 'center' }}>
                    <div>KSHRD Students</div>
                    <div>Sign in as <b>LEEHOU</b></div>
                </Col>
            </Row>
    )
}
