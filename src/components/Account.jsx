import React from 'react'
import { Col, Form, Image, Button } from 'react-bootstrap'

export default function Account(props) {


    return (

        <Col style={{ textAlign: 'center' }}>
            <br /><br />
            <Image style={{ width: '100px', height: '100px' }} src="./images/contact.png" roundedCircle />
            <br />
            <h4>Create User Account</h4>
            <Form style={{ textAlign: 'left' }} >
                <Form.Group controlId="formBasicName" >
                    <Form.Label>User Name</Form.Label>
                    <Form.Control onChange={(e) => props.onTextChange(e)}
                        type="text" placeholder="Enter UserName"
                        value={props.tempAccount.name}
                        />
                        <Form.Text className="text" style={{ color: 'red' }}>{props.message.nameErr}</Form.Text>
                </Form.Group>
                <br />
                    <Form.Label as="legend" column sm={2}>
                        Gender
                        </Form.Label>
                <Form.Group >
                    <Col sm={10}>
                        <Form.Check type="radio" inline checked={props.tempAccount.gender === "male"}
                            label="Male"
                            name="Radios"
                            id="radios1"
                            value="male"
                            onChange={(e) => props.onRadioChange(e)}
                        />
                        <Form.Check type="radio" inline checked={props.tempAccount.gender === "female"}
                            label="Female"
                            name="Radios"
                            id="radios2"
                            value="female"
                            onChange={(e) => props.onRadioChange(e)}
                        />
                    </Col>
                </Form.Group>
                <br />
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control onChange={(e) => props.onTextChange(e)}
                        type="email" placeholder="Enter email" 
                        value={props.tempAccount.email}
                        />
                    <Form.Text className="text" style={{ color: 'red' }}>{props.message.emailErr}</Form.Text>
                </Form.Group>
                <br />
                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control onChange={(e) => props.onTextChange(e)}
                        type="password" placeholder="Password" 
                        value={props.tempAccount.password}
                        />
                        <Form.Text className="text" style={{ color: 'red' }}>{props.message.passwordErr}</Form.Text>
                </Form.Group>
                <br />
                <Button variant="primary" type="button"
                    disabled={ props.onbtnCheck()}
                    onClick={ props.onAdd}
                >Save</Button>
            </Form>
        </Col>
    )
}
